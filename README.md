Based on [ConkyBar v3.2](http://n00by4ever.deviantart.com/art/ConkyBar-Conky-config-Conky-1-10-401254455 "ConkyBar v3.2")

# ChameleonBar

ChameleonBar is a stylish Conky bar which is easy to install. It is currently tailored specifically for openSUSE, but will include theming for other popular distributions as well. It is optimized for a resolution of 1920x1080. If you have larger or smaller monitor you will have to edit the skin manually.

## Prerequisites

* conky (v1.10)
* hddtemp (with the daemon running)
* sysstat

## Installing

TODO